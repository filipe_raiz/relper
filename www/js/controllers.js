angular.module('starter.controllers', [])

.controller('AppCtrl', ['$scope', '$stateParams', function($scope, $stateParams) {

}])


// LOGIN CONTROLLER

.controller('loginCtrl', ['$scope', '$state', '$http', '$ionicLoading', 'Auth', '$window', '$ionicPopup', 'Util', '$firebaseAuth', '$q', function($scope, $state, $http, $ionicLoading, Auth, $window, $ionicPopup, Util, $firebaseAuth, $q) {

    //$("#phone_n").mask('(99) 99999-9999');

    $scope.activeTemplate = 'login';
    $scope.user = {};

    if (Util.logged()) {
        $state.go('app.person');
    }

    var dadosUserFacebook = function(user_id){
        facebookConnectPlugin.api(user_id+"/?fields=id,name,email", ["email"],
        function (result) {
            userLoginFacebook(result);
        },
        function (error) {
            $ionicPopup.alert({
                title: '',
                template: 'Tente mais tarde!!!',
                buttons: [{
                    text: 'ok',
                    type: 'button-calm',
                }]
            });
            $ionicLoading.hide();
        });
    }

    var userLoginFacebook = function(result){
        $ionicLoading.show({template: 'Aguarde...'});
        var data = $.param({
            token_face: '#$bvhdsa%2@cqndv*&&nsadnsjDXFxaf4643hv12#2hsn563bcbjwCdSA3g2dj245%@j8',
            name:result.name,
            email:result.email,
            picture:"https://graph.facebook.com/" + result.id + "/picture?type=large"
        });

        var config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        }



        $http.post('http://realper.filiperaiz.com.br/api/v1/realper/login_facebook.json', data, config)
        .success(function(data, status, headers) {
            if ((typeof data.errors_user == "undefined") && data.flag_save_user.flag) { // DADOS PEGA
                $window.localStorage['user_token'] = JSON.stringify(data.user);
                $ionicLoading.hide();
                $state.go('app.person');
            } else if(!data.flag_save_user.flag) {
                var er = '';
                for (i = 0; i < data.errors_user.length; i++) {
                    er += data.errors_user[i].message + '<br>';
                }
                $ionicPopup.alert({
                    title: '',
                    template: er,
                    buttons: [{
                            text: 'ok',
                            type: 'button-calm',
                        }]
                });
                $ionicLoading.hide();
            }
        })
        .error(function(data, status, header, config) {
            $ionicPopup.alert({
                title: '',
                template: 'Tente mais tarde!!!',
                buttons: [{
                    text: 'ok',
                    type: 'button-calm',
                }]
            });
            $ionicLoading.hide();
        });
    }

    $scope.facebookLogin = function(){

        if (ionic.Platform.isIOS() || ionic.Platform.isAndroid()) {

            facebookConnectPlugin.login(['public_profile', 'email'], function(result) {
                dadosUserFacebook(result.authResponse.userID)
            }, function(error) {

                $ionicPopup.alert({
                    title: '',
                    template: 'Tente mais tarde!!!',
                    buttons: [{
                            text: 'ok',
                            type: 'button-calm',
                        }]
                });
                $ionicLoading.hide();
            });
        }
    }

    $scope.create_user = function() {

        $ionicLoading.show({
            template: 'Aguarde'
        });

        var credentials = {
            name: $scope.user.name,
            email: $scope.user.email,
            password: $scope.user.password,
            password_confirmation: $scope.user.password_confirmation
        };

        var config = {
            headers: {
                'X-HTTP-Method-Override': 'POST'
            }
        };

        Auth.register(credentials, config).then(function(registeredUser) {

        }, function(error) {
            message = '';
            if (typeof error.data.errors.name != 'undefined') {
                message += '<li>' + error.data.errors.name + '</li>'
            }
            if (typeof error.data.errors.email != 'undefined') {
                message += '<li>' + error.data.errors.email + '</li>'
            }
            if (typeof error.data.errors.password != 'undefined') {
                message += '<li>Senha ' + error.data.errors.password + '</li>'
            }
            $ionicPopup.alert({
                title: 'Aviso!!!',
                template: message,
                buttons: [{
                        text: 'ok',
                        type: 'button-calm',
                    }
                ]
            });
            $ionicLoading.hide();
        });

        $scope.$on('devise:new-registration', function(event, user) {
            $ionicLoading.hide();
            $ionicPopup.alert({
                title: 'Cadastro',
                template: 'Cadastro Realizado. Faça seu Login!!',
                buttons: [{
                        text: 'ok',
                        type: 'button-calm',
                    }
                ]
            });

            var config = {
                headers: {
                    'X-HTTP-Method-Override': 'DELETE'
                }
            };

            Auth.logout(config).then(function(oldUser) {
                // alert(oldUser.name + "you're signed out now.");
            }, function(error) {
                $ionicPopup.alert({
                    title: 'Aviso!!!',
                    template: 'Tente mais tarde!!!',
                    buttons: [{
                        text: 'ok',
                        type: 'button-calm',
                    }]
                });
                $ionicLoading.hide();
            });
            $scope.$on('devise:logout', function(event, oldCurrentUser) {
                // ...
                $ionicLoading.hide();
                $state.go('login');
            });
            $state.go('login');
        });
    };

    $scope.enter = function() {
        $ionicLoading.show({template: 'Aguarde...'});

        var credentials = {
            email: $scope.user.email,
            password: $scope.user.password
        };

        var config = {
            headers: {
                'X-HTTP-Method-Override': 'POST'
            }
        };

        Auth.login(credentials, config).then(function(user) {

        }, function(error) {
            $scope.error = 'Não foi possível fazer seu login. Tente novamente!!!';
            $ionicPopup.alert({
                title: 'Aviso!!!',
                template: $scope.error,
                buttons: [{
                        text: 'ok',
                        type: 'button-calm',
                    }]
            });
            $ionicLoading.hide();
        });

        $scope.$on('devise:login', function(event, currentUser) {
            // after a login, a hard refresh, a new tab
        });


        $scope.$on('devise:new-session', function(event, currentUser) {
            $window.localStorage['user_token'] = JSON.stringify(currentUser);
            $ionicLoading.hide();
            $state.go('app.person');
        });
    };

}])

// LOGOFF
.controller('logoffCtrl', ['$state', '$window', function($state, $window) {
    $window.localStorage.removeItem('user_token');
    $state.go('login');
}])

// MENU
.controller('menuCtrl', ['$scope', '$window', function($scope, $window) {
    $scope.user = JSON.parse($window.localStorage['user_token']);

    $scope.$on("menuUser", function (evt, data) {
        $scope.user = JSON.parse($window.localStorage['user_token']);
    });
}])

// TIMELINE CONTROLLER
.controller('TimelineCtrl', ['$scope', '$stateParams', 'Util', '$http', '$window', '$ionicLoading', function($scope, $stateParams, Util, $http, $window, $ionicLoading) {

    if (Util.logged()) {

        $scope.list_reminders = {};
        $scope.process = false;

        var user = JSON.parse($window.localStorage['user_token']);
        $scope.hasMoreData = true;
        $scope.page = 1;

        var parameters = {
            user_id: user.id,
            user_token: user.token,
            page: $scope.page
        };

        var config = {
            params: parameters
        };

        $http.get('http://realper.filiperaiz.com.br/api/v1/realper/list_reminders.json', config)
        .success(function(data, status, headers, config) {
            if (data.user_logged.flag) {
                $scope.list_reminders = data.list_reminders;
                $scope.process = true;
                $ionicLoading.hide();
            } else {
                $window.localStorage.removeItem('user_token');
                $ionicLoading.hide();
                $state.go('login');
            }
        });


        $scope.loadMore = function() {
            //$ionicLoading.show({template: '<ion-spinner icon="spiral"></ion-spinner><br>Aguarde...'});
            $scope.page += 1;

            var parameters = {
                token_user: user.token,
                user_id: user.id,
                user_token: user.token,
                page: $scope.page
            };

            var config = {
                params: parameters
            };

            $http.get('http://realper.filiperaiz.com.br/api/v1/realper/list_reminders.json', config)
            .success(function(data, status, headers, config) {
                if (data.user_logged.flag) {
                    if (data.list_reminders.length == 0) {
                        $scope.hasMoreData = false;
                    }
                    for (i = 0; i < data.list_reminders.length; i++) {
                        $scope.list_reminders.push(data.list_reminders[i]);
                    }
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                } else {
                    $window.localStorage.removeItem('user_token');
                    $ionicLoading.hide();
                    $state.go('login');
                }
            });
        };
    } else {
        $ionicLoading.hide();
        $state.go('login');
    }
}])



// PERSON CONTROLLER
.controller('ListPersonCtrl', ['$state', '$scope', '$stateParams', '$window', 'Util', '$ionicLoading', '$http', function($state, $scope, $stateParams, $window, Util, $ionicLoading, $http) {

    if (Util.logged()) {

        $scope.list_people = Array();
        $scope.process = false;

        var user = JSON.parse($window.localStorage['user_token']);
        $scope.hasMoreData = true;
        $scope.page = 1;

        var parameters = {
            user_id: user.id,
            user_token: user.token,
            page: $scope.page
        };

        var config = {
            params: parameters
        };

        $http.get('http://realper.filiperaiz.com.br/api/v1/realper/list_people.json', config)
        .success(function(data, status, headers, config) {
            if (data.user_logged.flag) {
                $scope.list_people = data.list_people;
                $scope.process = true;
                $ionicLoading.hide();
            } else {
                $window.localStorage.removeItem('user_token');
                $ionicLoading.hide();
                $state.go('login');
            }
        });


        $scope.loadMore = function() {
            //$ionicLoading.show({template: '<ion-spinner icon="spiral"></ion-spinner><br>Aguarde...'});
            $scope.page += 1;

            var parameters = {
                token_user: user.token,
                user_id: user.id,
                user_token: user.token,
                page: $scope.page
            };

            var config = {
                params: parameters
            };

            $http.get('http://realper.filiperaiz.com.br/api/v1/realper/list_people.json', config)
            .success(function(data, status, headers, config) {
                if (data.user_logged.flag) {
                    if (data.list_people.length == 0) {
                        $scope.hasMoreData = false;
                    }
                    for (i = 0; i < data.list_people.length; i++) {
                        $scope.list_people.push(data.list_people[i]);
                    }
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                } else {
                    $window.localStorage.removeItem('user_token');
                    $ionicLoading.hide();
                    $state.go('login');
                }
            });
        };

    } else {
        $ionicLoading.hide();
        $state.go('login');
    }
}])

.controller('itemPersonIdCtrl', ['$state', '$scope', '$stateParams', '$window', 'Util', '$ionicLoading', '$http', '$ionicScrollDelegate', '$cordovaCamera', '$ionicPopup', '$cordovaLocalNotification', function($state, $scope, $stateParams, $window, Util, $ionicLoading, $http, $ionicScrollDelegate, $cordovaCamera, $ionicPopup, $cordovaLocalNotification) {

    var photo_ant;
    //$("#phone_n").mask('(99) 99999-9999');
    $scope.person_id = $stateParams.person_id;

    if (Util.logged()) {

        $scope.list_people = {};

        var user = JSON.parse($window.localStorage['user_token']);
        $scope.hasMoreData = true;
        $scope.page = 1;

        var parameters = {
            user_id: user.id,
            user_token: user.token,
            person_id: $scope.person_id
        };

        var config = {
            params: parameters
        };

        $http.get('http://realper.filiperaiz.com.br/api/v1/realper/person.json', config)
        .success(function(data, status, headers, config) {
            if (data.user_logged.flag) {
                $scope.person = data.person;

                //console.log(data.person)

                //$scope.person.date_tmp = new Date(data.person.date_tmp);
                //$scope.person.date_tmp.setDate($scope.person.date_tmp.getDate() + 1);

                $scope.person.birth_date = new Date(data.person.birth_date);
                $scope.person.birth_date.setDate($scope.person.birth_date.getDate() + 1);

                $scope.person.when_relationship = new Date(data.person.when_relationship);
                $scope.person.when_relationship.setDate($scope.person.when_relationship.getDate() + 1);

                photo_ant = data.person.avatar;
                $ionicLoading.hide();
            } else {
                $window.localStorage.removeItem('user_token');
                $ionicLoading.hide();
                $state.go('login');
            }
        });

        $scope.delete = function(id) {
            try{
                var confirmPopup = $ionicPopup.confirm({
                    title: 'Deletar perfil',
                    template: 'Deseja realmente apagar esse perfil?',

                    buttons: [{
                            text: 'Sim',
                            type: 'button-calm',
                            onTap: function(res) {
                               if(res) {
                                   $ionicLoading.show({template: 'Aguarde...'});

                                   var parameters = {
                                       user_id: user.id,
                                       user_token: user.token,
                                       person_id: id
                                   };

                                   var config = {
                                       params: parameters
                                   };

                                   $http.delete('http://realper.filiperaiz.com.br/api/v1/realper/person_delete.json', config)
                                   .success(function(data, status, headers) {

                                        //console.log(data)
                                        for(i=0;i<data.reminders.length;i++){

                                            //alert(data.reminders[i].reminder_id)

                                            var id = parseInt("22222"+data.reminders[i].reminder_id.toString());
                                            $cordovaLocalNotification.isPresent(id).then(function (present) {
                                                if (present) {
                                                    $cordovaLocalNotification.cancel(id).then(function (result) {
                                                    });
                                                } else {
                                                }
                                            });

                                            for(j=0;j<data.reminders[i].alerts_ids.length;j++){
                                            
                                                //alert(data.reminders[i].alerts_ids[j].id)

                                                var id  = parseInt("2222"+data.reminders[i].alerts_ids[j].id.toString());
                                                $cordovaLocalNotification.isPresent(id).then(function (present) {
                                                    if (present) {
                                                        $cordovaLocalNotification.cancel(id).then(function (result) {
                                                        });
                                                    } else {
                                                    }
                                                });
                                            }
                                        }
                                       

                                        $ionicLoading.hide();
                                        $state.go('app.person');
                                   })
                                   .error(function(data, status, header, config) {

                                       $ionicPopup.alert({
                                           title: 'Aviso!!!',
                                           template: 'Ocorreu um erro!! Tente mais tarde.',
                                           buttons: [{
                                                   text: 'ok',
                                                   type: 'button-calm',
                                               }
                                           ]
                                       });
                                       $ionicLoading.hide();

                                   });
                               }else{

                               }
                            }
                        },

                        {
                            text: 'Não',
                            type: 'button-dark',

                        },
                    ]
                });

                // confirmPopup.then(function(res) {

                //     if(res) {
                //         $ionicLoading.show({template: 'Aguarde...'});

                //         var parameters = {
                //             user_id: user.id,
                //             user_token: user.token,
                //             person_id: id
                //         };

                //         var config = {
                //             params: parameters
                //         };

                //         $http.delete('http://realper.filiperaiz.com.br/api/v1/realper/person_delete.json', config)
                //         .success(function(data, status, headers) {
                //             $ionicLoading.hide();
                //             $state.go('app.person');
                //         })
                //         .error(function(data, status, header, config) {

                //             $ionicPopup.alert({
                //                 title: 'Aviso!!!',
                //                 template: 'Ocorreu um erro!! Tente mais tarde.',
                //                 buttons: [{
                //                         text: 'ok',
                //                         type: 'button-calm',
                //                     }
                //                 ]
                //             });
                //             $ionicLoading.hide();

                //         });
                //     }else{

                //     }
                // });
            } catch(erro) {
                $ionicPopup.alert({
                    title: 'Aviso!!!',
                    template: erro,
                    buttons: [{
                            text: 'ok',
                            type: 'button-calm',
                        }
                    ]
                });
                $ionicLoading.hide();
            }
        }

        $scope.save = function(id) {
            try{
                $ionicLoading.show({template: 'Aguarde'});

                if ($scope.person.avatar == 'img/upload/Icon-user.png' || $scope.person.avatar == photo_ant) {
                    avatar_img = '';
                } else {
                    avatar_img = $scope.person.avatar;
                }

                var data = $.param({
                    user_id: user.id,
                    user_token: user.token,
                    person_id: id,
                    avatar: avatar_img,
                    type_relative: $scope.person.type_relative,
                    as_met: $scope.person.as_met,
                    where_met: $scope.person.where_met,
                    when_relationship: $scope.person.when_relationship,
                    unforgettable_moments: $scope.person.unforgettable_moments,
                    something_else: $scope.person.something_else,
                    name: $scope.person.name,
                    birth_date: $scope.person.birth_date,
                    place_of_birth: $scope.person.place_of_birth,
                    qualities: $scope.person.qualities,
                    defects: $scope.person.defects,
                    color: $scope.person.color,
                    music: $scope.person.music,
                    films: $scope.person.films,
                    sports: $scope.person.sports,
                    place: $scope.person.place,
                    drinks: $scope.person.drinks,
                    books: $scope.person.books,
                    series: $scope.person.series,
                    food: $scope.person.food,
                    works_with_what: $scope.person.works_with_what,
                    job_time: $scope.person.job_time,
                    about_work: $scope.person.about_work,
                    reliable_friends: $scope.person.reliable_friends,
                    unreliable_friends: $scope.person.unreliable_friends,
                    name_mother: $scope.person.name_mother,
                    name_father: $scope.person.name_father,
                    brothers: $scope.person.brothers,
                    other_relatives: $scope.person.other_relatives,
                    shirt_size: $scope.person.shirt_size,
                    pant_size: $scope.person.pant_size,
                    short_size: $scope.person.short_size,
                    other_clothes: $scope.person.other_clothes,
                    nick: $scope.person.nick,
                    tatu: $scope.person.tatu,
                    piercing: $scope.person.piercing,
                    subject: $scope.person.subject,
                    attitudes: $scope.person.attitudes,
                    team: $scope.person.team,
                    animals: $scope.person.animals,
                    size_food: $scope.person.size_food,
                    dream: $scope.person.dream,
                    not_like: $scope.person.not_like,
                    date_tmp: $scope.person.date_tmp,
                    kids: $scope.person.kids,
                    cell_number:$scope.person.cell_number
                });

                var config = {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                    }
                }

                $http.post('http://realper.filiperaiz.com.br/api/v1/realper/person_save.json', data, config)
                .success(function(data, status, headers) {
                    if (typeof data.errors_person == "undefined") {

                            //$scope.generateNotification($scope.person.birth_date);
                            $scope.generateNotification($scope.person.birth_date, data.person.name, data.person.id);

                            $ionicLoading.hide();
                            $state.go('app.person');
                    } else {
                        var er = '';
                        for (i = 0; i < data.errors_person.length; i++) {
                            er += data.errors_person[i].message + '<br>';
                        }
                        $ionicPopup.alert({
                            title: 'Aviso!!!',
                            template: er,
                            buttons: [{
                                    text: 'ok',
                                    type: 'button-calm',
                                }
                            ]
                        });
                        $ionicLoading.hide();
                    }
                })
                .error(function(data, status, header, config) {
                    $ionicPopup.alert({
                        title: 'Aviso!!!',
                        template: 'Ocorreu um erro!! Tente mais tarde.',
                        buttons: [{
                                text: 'ok',
                                type: 'button-calm',
                            }
                        ]
                    });
                    $ionicLoading.hide();
                });
            } catch(erro) {
                $ionicPopup.alert({
                    title: 'Aviso!!!',
                    template: erro,
                    buttons: [{
                            text: 'ok',
                            type: 'button-calm',
                        }
                    ]
                });
                $ionicLoading.hide();
            }
        }

        var g = document.getElementsByClassName("acc-input");
        for (var i = 0; i < g.length; i++) {
            g[i].onclick = function() { $ionicScrollDelegate.resize(); };
        }

        $scope.takePhoto = function() {
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.CAMERA,
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 300,
                targetHeight: 300,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
            };

            $cordovaCamera.getPicture(options).then(function(imageData) {
                $scope.person.avatar = "data:image/jpeg;base64," + imageData;
            }, function(err) {
                // An error occured. Show a message to the user
            });
        }

        $scope.choosePhoto = function() {
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 300,
                targetHeight: 300,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
            };

            $cordovaCamera.getPicture(options).then(function(imageData) {
                $scope.person.avatar = "data:image/jpeg;base64," + imageData;
            }, function(err) {
                // An error occured. Show a message to the user
            });
        }

        $scope.generateNotification = function(date, name, id){
            //alert(date);
            //alert(id);
            //alert(name);
            var id = parseInt("22222"+id.toString());

            $cordovaLocalNotification.isPresent(id).then(function (present) {
                if (present) {
                    $cordovaLocalNotification.cancel(id).then(function (result) {
                    });
                } else {
                }
            });

            if (date != "undefined"){

                //alert(date);
                date = new Date(date);
                //alert(date);

                $cordovaLocalNotification.schedule({
                    id: id,
                    title: 'Aniversário',
                    text: 'Aniversário - '+name,
                    firstAt: date,
                    every: 'year'
                }).then(function (result) {
                    console.log('atualizado');
                });
            }
        }

        $scope.mascara = function(){
            //alert('--')
            mask = "## #####-####";
            if($scope.person.cell_number !== null && $scope.person.cell_number !== undefined){
                var i = $scope.person.cell_number.length;
                var saida = mask.substring(1,0);
                var texto = mask.substring(i)
                if (texto.substring(0,1) != saida){
                    $scope.person.cell_number+= texto.substring(0,1);
                }
            }
        }

    } else {
        $ionicLoading.hide();
        $state.go('login');
    }
}])

.controller('itemPersonNewCtrl', ['$state', '$scope', '$stateParams', '$cordovaCamera', '$ionicScrollDelegate', '$http', 'Util', '$window', '$ionicLoading', '$ionicPopup', '$cordovaLocalNotification', function($state, $scope, $stateParams, $cordovaCamera, $ionicScrollDelegate, $http, Util, $window, $ionicLoading, $ionicPopup, $cordovaLocalNotification) {
    //$("#phone_n").mask('(99) 99999-9999');

    if (Util.logged()) {

        $scope.person = {};

        $scope.add_person = function() {
            try{
                $ionicLoading.show({
                    template: 'Aguarde'
                });

                var user = JSON.parse($window.localStorage['user_token']);

                var data = $.param({
                    user_id: user.id,
                    user_token: user.token,
                    avatar: $scope.person.avatar,
                    type_relative: $scope.person.type_relative,
                    as_met: $scope.person.as_met,
                    where_met: $scope.person.where_met,
                    when_relationship: $scope.person.when_relationship,
                    unforgettable_moments: $scope.person.unforgettable_moments,
                    something_else: $scope.person.something_else,
                    name: $scope.person.name,
                    birth_date: $scope.person.birth_date,
                    place_of_birth: $scope.person.place_of_birth,
                    qualities: $scope.person.qualities,
                    defects: $scope.person.defects,
                    color: $scope.person.color,
                    music: $scope.person.music,
                    films: $scope.person.films,
                    sports: $scope.person.sports,
                    place: $scope.person.place,
                    drinks: $scope.person.drinks,
                    books: $scope.person.books,
                    series: $scope.person.series,
                    food: $scope.person.food,
                    works_with_what: $scope.person.works_with_what,
                    job_time: $scope.person.job_time,
                    about_work: $scope.person.about_work,
                    reliable_friends: $scope.person.reliable_friends,
                    unreliable_friends: $scope.person.unreliable_friends,
                    name_mother: $scope.person.name_mother,
                    name_father: $scope.person.name_father,
                    brothers: $scope.person.brothers,
                    other_relatives: $scope.person.other_relatives,
                    shirt_size: $scope.person.shirt_size,
                    pant_size: $scope.person.pant_size,
                    short_size: $scope.person.short_size,
                    other_clothes: $scope.person.other_clothes,
                    nick: $scope.person.nick,
                    tatu: $scope.person.tatu,
                    piercing: $scope.person.piercing,
                    subject: $scope.person.subject,
                    attitudes: $scope.person.attitudes,
                    team: $scope.person.team,
                    animals: $scope.person.animals,
                    size_food: $scope.person.size_food,
                    dream: $scope.person.dream,
                    not_like: $scope.person.not_like,
                    date_tmp: $scope.person.date_tmp,
                    kids: $scope.person.kids,
                    cell_number:$scope.person.cell_number
                });

                var config = {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                    }
                }


                $http.post('http://realper.filiperaiz.com.br/api/v1/realper/person_save.json', data, config)
                .success(function(data, status, headers, config) {

                    if (data.user_logged.flag) {
                        if (typeof data.errors_person == "undefined") {
                            $scope.medalalert(data.person_quant.quant);

                            $scope.generateNotification($scope.person.birth_date, data.person.name, data.person.id);

                            $ionicLoading.hide();
                            $state.go('app.person');
                        } else {
                            var er = '';
                            for (i = 0; i < data.errors_person.length; i++) {
                                er += data.errors_person[i].message + '<br>';
                            }
                            $ionicPopup.alert({
                                title: 'Aviso!!!',
                                template: er,
                                buttons: [{
                                        text: 'ok',
                                        type: 'button-calm',
                                    }
                                ]
                            });
                            $ionicLoading.hide();
                        }
                    } else {
                        $window.localStorage.removeItem('user_token');
                        $ionicLoading.hide();
                        $state.go('login');
                    }
                })
                .error(function(data, status, header, config) {
                    $ionicPopup.alert({
                        title: 'Aviso!!!',
                        template: 'Ocorreu um erro!! Tente mais tarde.',
                        buttons: [{
                                text: 'ok',
                                type: 'button-calm',
                            }
                        ]
                    });
                    $ionicLoading.hide();
                });
            } catch(erro) {
                $ionicPopup.alert({
                    title: 'Aviso!!!',
                    template: erro,
                    buttons: [{
                            text: 'ok',
                            type: 'button-calm',
                        }
                    ]
                });
                $ionicLoading.hide();
            }
        }

        var g = document.getElementsByClassName("acc-input");
        for (var i = 0; i < g.length; i++) {
            g[i].onclick = function() { $ionicScrollDelegate.resize(); };
        }

        $scope.takePhoto = function() {
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.CAMERA,
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 300,
                targetHeight: 300,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
            };

            $cordovaCamera.getPicture(options).then(function(imageData) {
                $scope.person.avatar = "data:image/jpeg;base64," + imageData;
            }, function(err) {
                // An error occured. Show a message to the user
            });
        }

        $scope.choosePhoto = function() {
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 300,
                targetHeight: 300,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
            };

            $cordovaCamera.getPicture(options).then(function(imageData) {
                $scope.person.avatar = "data:image/jpeg;base64," + imageData;
            }, function(err) {
                // An error occured. Show a message to the user
            });
        }

        $scope.medalalert = function(quant) {
            var str_alert,flag=false;
            if(quant==1){
                str_alert = '<div class="medal-conquer"><h2>Parabéns</h2><h4>Você ganhou uma medalha</h4><img src="img/upload/iniciando_relacionamento.png" alt=""><p>Iniciando relacionamento</p></div>';
                flag = true;
            }else if(quant==3){
                str_alert = '<div class="medal-conquer"><h2>Parabéns</h2><h4>Você ganhou uma medalha</h4><img src="img/upload/conquistador.png" alt=""><p>Conquistador</p></div>';
                flag = true;
            }
            if(flag){
                var alertPopup = $ionicPopup.alert({
                    title: '',
                    template: str_alert,
                    buttons: [{
                            text: 'ok',
                            type: 'button-calm',
                        }
                    ]
                });
            }
        }

        $scope.generateNotification = function(date, name, id){
            //alert(date);
            //alert(id);
            //alert(name);
            var id = parseInt("22222"+id.toString());


            $cordovaLocalNotification.isPresent(id).then(function (present) {
                if (present) {
                    $cordovaLocalNotification.cancel(id).then(function (result) {
                    });
                } else {
                }
            });


            if (date != "undefined"){

                //alert(date);
                date = new Date(date);
                //alert(date);

                $cordovaLocalNotification.schedule({
                    id: id,
                    title: 'Aniversário',
                    text: 'Aniversário - '+name,
                    firstAt: date,
                    every: 'year'
                }).then(function (result) {
                    console.log('atualizado');
                });
            }
        }

        $scope.mascara = function(){
            //alert('--')
            mask = "## #####-####";
            if($scope.person.cell_number !== null && $scope.person.cell_number !== undefined){
                var i = $scope.person.cell_number.length;
                var saida = mask.substring(1,0);
                var texto = mask.substring(i)
                if (texto.substring(0,1) != saida){
                    $scope.person.cell_number+= texto.substring(0,1);
                }
            }
        }

    } else {
        $ionicLoading.hide();
        $state.go('login');
    }
}])



// REMINDER
.controller('itemPersonDateCtrl', ['$state', '$scope', '$stateParams', '$cordovaCamera', '$ionicScrollDelegate', '$http', 'Util', '$window', '$ionicLoading', '$ionicPopup', function($state, $scope, $stateParams, $cordovaCamera, $ionicScrollDelegate, $http, Util, $window, $ionicLoading, $ionicPopup) {

    $scope.person_id = $stateParams.person_id;
    $scope.process = false;

    if (Util.logged()) {

        $scope.list_reminders = Array();

        var user = JSON.parse($window.localStorage['user_token']);
        $scope.hasMoreData = true;
        $scope.page = 1;

        var parameters = {
            user_id: user.id,
            user_token: user.token,
            person_id: $scope.person_id,
            page: $scope.page
        };

        var config = {
            params: parameters
        };

        $http.get('http://realper.filiperaiz.com.br/api/v1/realper/list_reminders.json', config)
            .success(function(data, status, headers, config) {
                if (data.user_logged.flag) {
                    $scope.list_reminders = data.list_reminders;
                    $scope.process = true;
                    $ionicLoading.hide();
                } else {
                    $window.localStorage.removeItem('user_token');
                    $ionicLoading.hide();
                    $state.go('login');
                }
            });


        $scope.loadMore = function() {
            //$ionicLoading.show({template: '<ion-spinner icon="spiral"></ion-spinner><br>Aguarde...'});
            $scope.page += 1;

            var parameters = {
                token_user: user.token,
                user_id: user.id,
                user_token: user.token,
                person_id: $scope.person_id,
                page: $scope.page
            };

            var config = {
                params: parameters
            };

            $http.get('http://realper.filiperaiz.com.br/api/v1/realper/list_reminders.json', config)
            .success(function(data, status, headers, config) {
                if (data.user_logged.flag) {
                    if (data.list_reminders.length == 0) {
                        $scope.hasMoreData = false;
                    }
                    for (i = 0; i < data.list_reminders.length; i++) {
                        $scope.list_reminders.push(data.list_reminders[i]);
                    }
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                } else {
                    $window.localStorage.removeItem('user_token');
                    $ionicLoading.hide();
                    $state.go('login');
                }
            });
        };

    } else {
        $ionicLoading.hide();
        $state.go('login');
    }
}])

.controller('itemPersonDateIdCtrl', ['$state', '$scope', '$stateParams', '$cordovaCamera', '$ionicScrollDelegate', '$http', 'Util', '$window', '$ionicLoading', '$ionicPopup', '$cordovaLocalNotification', function($state, $scope, $stateParams, $cordovaCamera, $ionicScrollDelegate, $http, Util, $window, $ionicLoading, $ionicPopup, $cordovaLocalNotification) {

    $scope.reminder_id = $stateParams.reminder_id;
    
    if (Util.logged()) {

        $scope.reminder = {};

        var user = JSON.parse($window.localStorage['user_token']);
        $scope.hasMoreData = true;

        var parameters = {
            user_id: user.id,
            user_token: user.token,
            reminder_id: $scope.reminder_id
        };

        var config = {
            params: parameters
        };

        $http.get('http://realper.filiperaiz.com.br/api/v1/realper/reminder.json', config)
        .success(function(data, status, headers, config) {
            if (data.user_logged.flag) {

                $scope.reminder = data.reminder;
                $scope.reminder.date = new Date($scope.reminder.date);
                $scope.reminder.date.setDate($scope.reminder.date.getDate());

                $scope.reminder.element_alerts  = new Array();
                $scope.reminder.dates           = new Array();
                $scope.reminder.times           = new Array();

                for(i=0;i<$scope.reminder.alerts.length;i++){
                    $scope.reminder.element_alerts.push(i);
                    $scope.reminder.dates.push(new Date($scope.reminder.alerts[i].date));
                    $scope.reminder.times.push(new Date($scope.reminder.alerts[i].date));
                }


                $ionicLoading.hide();
            } else {
                $window.localStorage.removeItem('user_token');
                $ionicLoading.hide();
                $state.go('login');
            }
        });


        $scope.delete = function(id) {
            try{
                $ionicLoading.show({template: 'Aguarde...'});

                //REMOVE OS ALERTS
                //for(i=0;i<$scope.reminder.reminder_alerts.length;i++){
                //    var id = $scope.reminder.id+'alerta'+$scope.reminder.reminder_alerts[i].id;
                //    $cordovaLocalNotification.cancel(id).then(function (result) {});
                //}

                var parameters = {
                    user_id: user.id,
                    user_token: user.token,
                    reminder_id: id
                };

                var config = {
                    params: parameters
                };

                $http.delete('http://realper.filiperaiz.com.br/api/v1/realper/reminder_delete.json', config)
                .success(function(data, status, headers) {

                    //CANCELANDO LEMBRETES PASSADOS
                    for(i=0;i<data.alerts.length;i++){
                        
                        var id  = parseInt("2222"+data.alerts[i].id.toString());

                        //alert(id)

                        $cordovaLocalNotification.isPresent(id).then(function (present) {
                            if (present) {
                                $cordovaLocalNotification.cancel(id).then(function (result) {
                                    //console.log('Notification EveryMinute Cancelled');
                                    //alert('Cancelled Every Minute');
                                });
                            } else {
                                //alert("notificacao encontrada recorrente");
                            }
                        });
                    }

                    var id  = parseInt("22222"+data.reminder_alert.id.toString());
                    //alert(id)

                    $cordovaLocalNotification.isPresent(id).then(function (present) {
                        if (present) {
                            $cordovaLocalNotification.cancel(id).then(function (result) {
                                //console.log('Notification EveryMinute Cancelled');
                                //alert('Cancelled Every Minute');
                            });
                        } else {
                            //alert("notificacao encontrada recorrente");
                        }
                    });




                    $ionicLoading.hide();
                    $state.go('app.person_date', { person_id: data.person.id });
                })
                .error(function(data, status, header, config) {

                });
            } catch(erro) {
                $ionicPopup.alert({
                    title: 'Aviso!!!',
                    template: erro,
                    buttons: [{
                            text: 'ok',
                            type: 'button-calm',
                        }
                    ]
                });
                $ionicLoading.hide();
            }
        }

        $scope.save = function(id) {

            $scope.reminder.id = id;

            var flag_recorrente;
            if($scope.reminder.recorrente){
                flag_recorrente = 1;
            }else{
                flag_recorrente = 0;
            }
            

            try{
                $ionicLoading.show({template: 'Aguarde'});

                if($scope.reminder.recorrente === undefined){
                    $scope.reminder.recorrente = false;
                }

                var aler = new Array();
                for(i=0;i<$scope.reminder.dates.length;i++){
                    var da = new Date($scope.reminder.dates[i]);
                    var te = new Date($scope.reminder.times[i]);

                    var ano = da.getFullYear();
                    var mes = da.getMonth()+1;
                    var dia = da.getDate();

                    var hora = te.getHours();
                    var minu = te.getMinutes();

                    var str = ano+'/'+mes+'/'+dia+' '+hora+':'+minu+':00';

                    //alert(str)

                    aler.push(str);
                }

                var data = $.param({
                    user_id: user.id,
                    user_token: user.token,
                    reminder_id: id,
                    date: $scope.reminder.date,
                    recorrente: flag_recorrente,
                    description: $scope.reminder.description,
                    reminder_alerts:aler
                });

                var config = {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                    }
                }

                $http.post('http://realper.filiperaiz.com.br/api/v1/realper/reminder_save.json', data, config)
                .success(function(data, status, headers) {
                    if (typeof data.errors_reminder == "undefined") {

                        $scope.notifyLocal(data.alerts_ids, $scope.reminder.description, $scope.reminder.id);

                        $ionicLoading.hide();
                        $state.go('app.person_date', { person_id: data.person.id });
                    } else {
                        var er = '';
                        for (i = 0; i < data.errors_reminder.length; i++) {
                            er += data.errors_reminder[i].message + '<br>';
                        }
                        $ionicPopup.alert({
                            title: 'Aviso!!!',
                            template: er,
                            buttons: [{
                                    text: 'ok',
                                    type: 'button-calm',
                                }
                            ]
                        });
                        $ionicLoading.hide();
                    }
                })
                .error(function(data, status, header, config) {

                });
            } catch(erro) {
                $ionicPopup.alert({
                    title: 'Aviso!!!',
                    template: erro,
                    buttons: [{
                            text: 'ok',
                            type: 'button-calm',
                        }
                    ]
                });
                $ionicLoading.hide();
            }
        }

        $scope.moreAlert = function() {
            $scope.reminder.element_alerts.push($scope.reminder.element_alerts.length + 1);
        }

        $scope.notifyLocal = function(notify, descr, reminder_id) {
            try{

                // RECORRENTE
                if($scope.reminder.recorrente){

                    var dd = new Date($scope.reminder.date).getTime();
                    var d_ = new Date(dd);
                    var id = parseInt("22222"+reminder_id.toString());




                    var newDate = d_;

                    newDate.setDate(newDate.getDate()-15);
                    newDate.setHours(10);


                    $cordovaLocalNotification.isPresent(id).then(function (present) {
                        if (present) {
                            $cordovaLocalNotification.cancel(id).then(function (result) {
                            });
                        } else {
                            //alert("notificacao encontrada recorrente");
                        }
                    });

                    $cordovaLocalNotification.add({
                        id: id,
                        date: $scope.reminder.date,
                        message: "Alerta",
                        title: descr,
                        firstAt: newDate,
                        every: "day"
                    }).then(function () {
                        console.log("Notificação agendada");
                        //alert("notificacao recorrente agendada");
                    });
                }else{

                    var id = parseInt("22222"+reminder_id.toString());

                    $cordovaLocalNotification.isPresent(id).then(function (present) {
                        if (present) {
                            $cordovaLocalNotification.cancel(id).then(function (result) {
                            });
                        } else {
                            //alert("notificacao encontrada");
                        }
                    });
                }

                //CANCELANDO LEMBRETES PASSADOS
                for(i=0;i<$scope.reminder.alerts.length;i++){
                    
                    var id  = parseInt("2222"+$scope.reminder.alerts[i].id.toString());


                    $cordovaLocalNotification.isPresent(id).then(function (present) {
                        if (present) {
                            $cordovaLocalNotification.cancel(id).then(function (result) {
                            });
                        } else {
                        }
                    });
                }



                // PARA OS LEMBRETES
                for(i=0;i<notify.length;i++){

                    var now = new Date(notify[i].date);
                    var d   = new Date(now);
                    var id  = parseInt("2222"+notify[i].id.toString());

                    //alert(id)

                    //alert("--")
                    //alert(d)

                    d.setHours(d.getHours()+1);

                    //alert(d)

                    $cordovaLocalNotification.add({
                        id: id,
                        date: d,
                        message: "Alerta",
                        title: descr
                    }).then(function () {
                        console.log("Notificação agendada");
                        //alert("Notificação agendada")
                        //alert(d)
                    });
                    
                }

                /*
                // PARA LEMBRETES
                for(i=0;i<notify.length;i++){

                    var d = new Date(notify[i].date);
                    var id  = parseInt("2222"+notify[i].id.toString());

                    $cordovaLocalNotification.add({
                        id: id,
                        date: d,
                        message: "Alerta",
                        title: descr
                    }).then(function () {
                        console.log("Notificação agendada");
                       // alert("notificacao agendada");
                    });
                }
                */
            } catch(erro) {
                $ionicPopup.alert({
                    title: 'Aviso!!!',
                    template: erro,
                    buttons: [{
                            text: 'ok',
                            type: 'button-calm',
                        }]
                });
                $ionicLoading.hide();
            }
        }

    } else {
        $ionicLoading.hide();
        $state.go('login');
    }
}])

.controller('itemPersonDateNewCtrl', ['$state', '$scope', '$stateParams', '$cordovaCamera', '$ionicScrollDelegate', '$http', 'Util', '$window', '$ionicLoading', '$ionicPopup', '$cordovaLocalNotification', function($state, $scope, $stateParams, $cordovaCamera, $ionicScrollDelegate, $http, Util, $window, $ionicLoading, $ionicPopup, $cordovaLocalNotification) {

    if (Util.logged()) {

        $scope.reminder = {};

        $scope.reminder.element_alerts   = [0];
        $scope.reminder.times           = new Array();
        $scope.reminder.dates           = new Array();

        $scope.reminder.al           = new Array();


        var user = JSON.parse($window.localStorage['user_token']);
        var person_id = $stateParams.person_id;


        $scope.save = function() {

            if($scope.reminder.recorrente === undefined){
                $scope.reminder.recorrente = false;
            }


            var flag_recorrente;
            if($scope.reminder.recorrente){
                flag_recorrente = 1;
            }else{
                flag_recorrente = 0;
            }


            //alert($scope.reminder.recorrente)

            try{
                $ionicLoading.show({ template: 'Aguarde...' });

                var aler = new Array();
                for(i=0;i<$scope.reminder.dates.length;i++){
                    
                    var da = new Date($scope.reminder.dates[i]);
                    var te = new Date($scope.reminder.times[i]);

                    var ano = da.getFullYear();
                    var mes = da.getMonth()+1;
                    var dia = da.getDate();

                    var hora = te.getHours();
                    var minu = te.getMinutes();

                    var str = ano+'/'+mes+'/'+dia+' '+hora+':'+minu+':00';

                    //alert(str)

                    aler.push(str);
                }

                var data = $.param({
                    user_id: user.id,
                    user_token: user.token,
                    date: $scope.reminder.date,
                    person_id: person_id,
                    recorrente: flag_recorrente,
                    description: $scope.reminder.description,
                    reminder_alerts:aler
                });

                var config = {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                    }
                }

                $http.post('http://realper.filiperaiz.com.br/api/v1/realper/reminder_save.json', data, config)
                .success(function(data, status, headers) {
                    if (typeof data.errors_date == "undefined") {

                        if (typeof data.errors_reminder == "undefined") {
                            $scope.notifyLocal(data.alerts_ids, $scope.reminder.description, data.person.reminder_id);
                            $scope.medalalert(data.reminder_quant.quant);
                            $ionicLoading.hide();
                            $state.go('app.person_date', { person_id: data.person.id });
                        } else {
                            var er = '';
                            for (i = 0; i < data.errors_reminder.length; i++) {
                                er += data.errors_reminder[i].message + '<br>';
                            }
                            $ionicPopup.alert({
                                title: 'Aviso!!!',
                                template: er,
                                buttons: [{
                                        text: 'ok',
                                        type: 'button-calm',
                                    }]
                            });
                            $ionicLoading.hide();
                        }
                    } else {
                        var er = '';
                        for (i = 0; i < data.errors_person.length; i++) {
                            er += data.errors_person[i].message + '<br>';
                        }
                        $ionicPopup.alert({
                            title: 'Aviso!!!',
                            template: er,
                            buttons: [{
                                    text: 'ok',
                                    type: 'button-calm',
                                }
                            ]
                        });
                        $ionicLoading.hide();
                    }

                })
                .error(function(data, status, header, config) {
                    $ionicPopup.alert({
                        title: 'Aviso!!!',
                        template: 'Tente mais tarde.',
                        buttons: [{
                                text: 'ok',
                                type: 'button-calm',
                            }]
                    });
                    $ionicLoading.hide();
                });
            } catch(erro) {
                $ionicPopup.alert({
                    title: 'Aviso!!!',
                    template: erro,
                    buttons: [{
                            text: 'ok',
                            type: 'button-calm',
                        }]
                });
                $ionicLoading.hide();
            }
        }

        $scope.moreAlert = function() {
            $scope.reminder.element_alerts.push($scope.reminder.element_alerts.length + 1);
        }

        $scope.notifyLocal = function(notify, descr, reminder_id) {
            try{

                // RECORRENTE
                if($scope.reminder.recorrente){
                    
                    //alert($scope.reminder.date);

                    var dd = new Date($scope.reminder.date).getTime();
                    var d_ = new Date(dd);
                    var id = parseInt("22222"+reminder_id.toString());


                    

                    //alert(d_);

                    var newDate = d_;

                    newDate.setDate(newDate.getDate()-15);
                    newDate.setHours(10);

                    
                    //alert(newDate)
                    //alert($scope.reminder.date)

                    //alert(id)

                    $cordovaLocalNotification.add({
                        id: id,
                        date: $scope.reminder.date,
                        message: "Alerta Recorrente",
                        title: descr,
                        firstAt: newDate,
                        every: "day"
                    }).then(function () {
                        console.log("Notificação agendada");
                        //alert("Notificação agendada recorrente");
                    });
                }else{

                    var id = parseInt("22222"+reminder_id.toString());
                    //alert(id)

                    
                    $cordovaLocalNotification.isPresent(id).then(function (present) {
                        if (present) {
                            $cordovaLocalNotification.cancel(id).then(function (result) {
                            });
                        } else {
                            //alert("n existe")
                        }
                    });
                }

                // PARA OS LEMBRETES
                for(i=0;i<notify.length;i++){

                    var now = new Date(notify[i].date).getTime();
                    var d   = new Date(now);
                    var id  = parseInt("2222"+notify[i].id.toString());

                    //alert(id)

                    //alert("--")
                    //alert(d)

                    $cordovaLocalNotification.add({
                        id: id,
                        date: d,
                        message: "Alerta",
                        title: descr
                    }).then(function () {
                        console.log("Notificação agendada");
                        //alert("Notificação agendada")
                        //alert(d)
                    });
                    
                }
            } catch(erro) {
                $ionicPopup.alert({
                    title: 'Aviso!!!',
                    template: erro,
                    buttons: [{
                            text: 'ok',
                            type: 'button-calm',
                        }]
                });
                $ionicLoading.hide();
            }
        }

        $scope.medalalert = function(quant) {
            var str_alert,flag=false;
            if(quant==5){
                str_alert = '<div class="medal-conquer"><h2>Parabéns</h2><h4>Você ganhou uma medalha</h4><img src="img/upload/comprometido.png" alt=""><p>Comprometido</p></div>';
                flag = true;
            }
            if(flag){
                var alertPopup = $ionicPopup.alert({
                    title: '',
                    template: str_alert,
                    buttons: [{
                            text: 'ok',
                            type: 'button-calm',
                        }
                    ]
                });
            }
        }

    } else {
        $ionicLoading.hide();
        $state.go('login');
    }
}])



// PLACE
.controller('itemPersonPlaceCtrl', ['$state', '$scope', '$stateParams', '$cordovaCamera', '$ionicScrollDelegate', '$http', 'Util', '$window', '$ionicLoading', '$ionicPopup', function($state, $scope, $stateParams, $cordovaCamera, $ionicScrollDelegate, $http, Util, $window, $ionicLoading, $ionicPopup) {

    $scope.process = false;
    $scope.person_id = $stateParams.person_id;


    if (Util.logged()) {

        $scope.list_places = Array();

        var user = JSON.parse($window.localStorage['user_token']);
        $scope.hasMoreData = true;
        $scope.page = 1;

        var parameters = {
            user_id: user.id,
            user_token: user.token,
            person_id: $scope.person_id,
            page: $scope.page
        };

        var config = {
            params: parameters
        };

        $http.get('http://realper.filiperaiz.com.br/api/v1/realper/list_places.json', config)
        .success(function(data, status, headers, config) {
            if (data.user_logged.flag) {
                $scope.list_places = data.list_places;
                $scope.process = true;
                $ionicLoading.hide();
            } else {
                $window.localStorage.removeItem('user_token');
                $ionicLoading.hide();
                $state.go('login');
            }
        });


        $scope.loadMore = function() {
            //$ionicLoading.show({template: '<ion-spinner icon="spiral"></ion-spinner><br>Aguarde...'});
            $scope.page += 1;

            var parameters = {
                token_user: user.token,
                user_id: user.id,
                user_token: user.token,
                person_id: $scope.person_id,
                page: $scope.page
            };

            var config = {
                params: parameters
            };

            $http.get('http://realper.filiperaiz.com.br/api/v1/realper/list_places.json', config)
                .success(function(data, status, headers, config) {
                    if (data.user_logged.flag) {
                        if (data.list_places.length == 0) {
                            $scope.hasMoreData = false;
                        }
                        for (i = 0; i < data.list_places.length; i++) {
                            $scope.list_places.push(data.list_places[i]);
                        }
                        $scope.$broadcast('scroll.infiniteScrollComplete');
                    } else {
                        $window.localStorage.removeItem('user_token');
                        $ionicLoading.hide();
                        $state.go('login');
                    }
                });
        };


    } else {
        $ionicLoading.hide();
        $state.go('login');
    }
}])

.controller('itemPersonPlaceIdCtrl', ['$state', '$scope', '$stateParams', '$cordovaCamera', '$ionicScrollDelegate', '$http', 'Util', '$window', '$ionicLoading', '$ionicPopup', function($state, $scope, $stateParams, $cordovaCamera, $ionicScrollDelegate, $http, Util, $window, $ionicLoading, $ionicPopup) {

    $scope.place_id = $stateParams.place_id;
    var photo_ant;

    if (Util.logged()) {

        $scope.person = {};

        var user = JSON.parse($window.localStorage['user_token']);
        $scope.hasMoreData = true;

        var parameters = {
            user_id: user.id,
            user_token: user.token,
            place_id: $scope.place_id
        };

        var config = {
            params: parameters
        };

        $http.get('http://realper.filiperaiz.com.br/api/v1/realper/place.json', config)
        .success(function(data, status, headers, config) {
            if (data.user_logged.flag) {
                $scope.place = data.place;
                photo_ant = data.place.image;

                $scope.place.visit_date = new Date(data.place.visit_date);
                $scope.place.visit_date.setDate($scope.place.visit_date.getDate() + 1);

                
                $ionicLoading.hide();
            } else {
                $window.localStorage.removeItem('user_token');
                $ionicLoading.hide();
                $state.go('login');
            }
        });


        $scope.delete = function(id) {
            try{
                $ionicLoading.show({ template: 'Aguarde...' });
                var parameters = {
                    user_id: user.id,
                    user_token: user.token,
                    place_id: id
                };

                var config = {
                    params: parameters
                };

                $http.delete('http://realper.filiperaiz.com.br/api/v1/realper/place_delete.json', config)
                .success(function(data, status, headers) {
                    $ionicLoading.hide();
                    $state.go('app.person_place', { person_id: data.person.id });
                })
                .error(function(data, status, header, config) {

                });
            } catch(erro) {
                $ionicPopup.alert({
                    title: 'Aviso!!!',
                    template: erro,
                    buttons: [{
                            text: 'ok',
                            type: 'button-calm',
                        }
                    ]
                });
                $ionicLoading.hide();
            }
        }

        $scope.save = function(id) {
            try{
                $ionicLoading.show({ template: 'Aguarde...' });

                if ($scope.place.image == 'img/upload/icon-camera.png' || $scope.place.image == photo_ant) {
                    photo_img = '';
                } else {
                    photo_img = $scope.place.image;
                }


                var data = $.param({
                    user_id: user.id,
                    user_token: user.token,
                    place_id: id,
                    title: $scope.place.title,
                    visit_date: $scope.place.visit_date,
                    rating: $scope.place.rating,
                    description: $scope.place.description,
                    image: photo_img
                });

                var config = {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                    }
                }

                $http.post('http://realper.filiperaiz.com.br/api/v1/realper/place_save.json', data, config)
                .success(function(data, status, headers) {
                    if (typeof data.errors_place == "undefined") {
                        $ionicLoading.hide();
                        $state.go('app.person_place', { person_id: data.person.id });
                    } else {
                        var er = '';
                        for (i = 0; i < data.errors_place.length; i++) {
                            er += data.errors_place[i].message + '<br>';
                        }
                        $ionicPopup.alert({
                            title: 'Aviso!!!',
                            template: er,
                            buttons: [{
                                    text: 'ok',
                                    type: 'button-calm',
                                }
                            ]
                        });
                        $ionicLoading.hide();
                    }
                })
                .error(function(data, status, header, config) {

                });
            } catch(erro) {
                $ionicPopup.alert({
                    title: 'Aviso!!!',
                    template: erro,
                    buttons: [{
                            text: 'ok',
                            type: 'button-calm',
                        }
                    ]
                });
                $ionicLoading.hide();
            }
        }

        $scope.takePhoto = function() {
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.CAMERA,
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 300,
                targetHeight: 300,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
            };

            $cordovaCamera.getPicture(options).then(function(imageData) {
                $scope.place.image = "data:image/jpeg;base64," + imageData;
            }, function(err) {
                // An error occured. Show a message to the user
            });
        }

        $scope.choosePhoto = function() {
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 300,
                targetHeight: 300,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
            };

            $cordovaCamera.getPicture(options).then(function(imageData) {
                $scope.place.image = "data:image/jpeg;base64," + imageData;
            }, function(err) {
                // An error occured. Show a message to the user
            });
        }

    } else {
        $ionicLoading.hide();
        $state.go('login');
    }
}])

.controller('itemPersonPlaceNewCtrl', ['$state', '$scope', '$stateParams', '$cordovaCamera', '$ionicScrollDelegate', '$http', 'Util', '$window', '$ionicLoading', '$ionicPopup', function($state, $scope, $stateParams, $cordovaCamera, $ionicScrollDelegate, $http, Util, $window, $ionicLoading, $ionicPopup) {

    if (Util.logged()) {

        $scope.place = {};

        var user = JSON.parse($window.localStorage['user_token']);
        var person_id = $stateParams.person_id;


        $scope.save = function() {
            try{
                $ionicLoading.show({ template: 'Aguarde...' });
                var data = $.param({
                    user_id: user.id,
                    user_token: user.token,
                    title: $scope.place.title,
                    description: $scope.place.description,
                    image: $scope.place.image,
                    visit_date: $scope.place.visit_date,
                    rating: $scope.place.rating,
                    person_id: person_id
                });

                var config = {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                    }
                }

                $http.post('http://realper.filiperaiz.com.br/api/v1/realper/place_save.json', data, config)
                .success(function(data, status, headers) {

                    if (typeof data.errors_place == "undefined") {
                        $scope.medalalert(data.place_quant.quant);
                        $ionicLoading.hide();
                        $state.go('app.person_place', { person_id: data.person.id });
                    } else {
                        var er = '';
                        for (i = 0; i < data.errors_place.length; i++) {
                            er += data.errors_place[i].message + '<br>';
                        }
                        $ionicPopup.alert({
                            title: 'Aviso!!!',
                            template: er,
                            buttons: [{
                                    text: 'ok',
                                    type: 'button-calm',
                                }
                            ]
                        });
                        $ionicLoading.hide();
                    }
                })
                .error(function(data, status, header, config) {

                });
            } catch(erro) {
                $ionicPopup.alert({
                    title: 'Aviso!!!',
                    template: erro,
                    buttons: [{
                            text: 'ok',
                            type: 'button-calm',
                        }
                    ]
                });
                $ionicLoading.hide();
            }
        }

        $scope.takePhoto = function() {
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.CAMERA,
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 300,
                targetHeight: 300,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
            };

            $cordovaCamera.getPicture(options).then(function(imageData) {
                $scope.place.image = "data:image/jpeg;base64," + imageData;
            }, function(err) {
                // An error occured. Show a message to the user
            });
        }

        $scope.choosePhoto = function() {
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 300,
                targetHeight: 300,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
            };

            $cordovaCamera.getPicture(options).then(function(imageData) {
                $scope.place.image = "data:image/jpeg;base64," + imageData;
            }, function(err) {
                // An error occured. Show a message to the user
            });
        }

        $scope.medalalert = function(quant) {
            var str_alert,flag=false;
            if(quant==1){
                str_alert = '<div class="medal-conquer"><h2>Parabéns</h2><h4>Você ganhou uma medalha</h4><img src="img/upload/explorador.png" alt=""><p>Explorador</p></div>';
                flag = true;
            }
            if(flag){
                var alertPopup = $ionicPopup.alert({
                    title: '',
                    template: str_alert,
                    buttons: [{
                            text: 'ok',
                            type: 'button-calm',
                        }
                    ]
                });
            }
        }

    } else {
        $ionicLoading.hide();
        $state.go('login');
    }
}])



// FOOD
.controller('itemPersonFoodCtrl', ['$state', '$scope', '$stateParams', '$cordovaCamera', '$ionicScrollDelegate', '$http', 'Util', '$window', '$ionicLoading', '$ionicPopup', function($state, $scope, $stateParams, $cordovaCamera, $ionicScrollDelegate, $http, Util, $window, $ionicLoading, $ionicPopup) {

    $scope.process = false;
    $scope.person_id = $stateParams.person_id;

    if (Util.logged()) {

        $scope.list_foods = Array();

        var user = JSON.parse($window.localStorage['user_token']);
        $scope.hasMoreData = true;
        $scope.page = 1;

        var parameters = {
            user_id: user.id,
            user_token: user.token,
            person_id: $scope.person_id,
            page: $scope.page
        };

        var config = {
            params: parameters
        };

        $http.get('http://realper.filiperaiz.com.br/api/v1/realper/list_foods.json', config)
        .success(function(data, status, headers, config) {
            if (data.user_logged.flag) {
                $scope.list_foods = data.list_foods;
                $scope.process = true;
                $ionicLoading.hide();
            } else {
                $window.localStorage.removeItem('user_token');
                $ionicLoading.hide();
                $state.go('login');
            }
        });


        $scope.loadMore = function() {
            //$ionicLoading.show({template: '<ion-spinner icon="spiral"></ion-spinner><br>Aguarde...'});
            $scope.page += 1;

            var parameters = {
                token_user: user.token,
                user_id: user.id,
                user_token: user.token,
                person_id: $scope.person_id,
                page: $scope.page
            };

            var config = {
                params: parameters
            };

            $http.get('http://realper.filiperaiz.com.br/api/v1/realper/list_foods.json', config)
            .success(function(data, status, headers, config) {
                if (data.user_logged.flag) {
                    if (data.list_foods.length == 0) {
                        $scope.hasMoreData = false;
                    }
                    for (i = 0; i < data.list_foods.length; i++) {
                        $scope.list_foods.push(data.list_foods[i]);
                    }
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                } else {
                    $window.localStorage.removeItem('user_token');
                    $ionicLoading.hide();
                    $state.go('login');
                }
            });
        };


    } else {
        $ionicLoading.hide();
        $state.go('login');
    }
}])

.controller('itemPersonFoodIdCtrl', ['$state', '$scope', '$stateParams', '$cordovaCamera', '$ionicScrollDelegate', '$http', 'Util', '$window', '$ionicLoading', '$ionicPopup', function($state, $scope, $stateParams, $cordovaCamera, $ionicScrollDelegate, $http, Util, $window, $ionicLoading, $ionicPopup) {

    $scope.food_id = $stateParams.food_id;
    var photo_ant;

    if (Util.logged()) {


        var user = JSON.parse($window.localStorage['user_token']);
        $scope.hasMoreData = true;

        var parameters = {
            user_id: user.id,
            user_token: user.token,
            food_id: $scope.food_id
        };

        var config = {
            params: parameters
        };

        $http.get('http://realper.filiperaiz.com.br/api/v1/realper/food.json', config)
        .success(function(data, status, headers, config) {
            if (data.user_logged.flag) {
                $scope.food = data.food;
                photo_ant = data.food.image;
                $ionicLoading.hide();
            } else {
                $window.localStorage.removeItem('user_token');
                $ionicLoading.hide();
                $state.go('login');
            }
        });


        $scope.delete = function(id) {
            try{
                $ionicLoading.show({ template: 'Aguarde...' });
                var parameters = {
                    user_id: user.id,
                    user_token: user.token,
                    food_id: id
                };

                var config = {
                    params: parameters
                };

                $http.delete('http://realper.filiperaiz.com.br/api/v1/realper/food_delete.json', config)
                .success(function(data, status, headers) {
                    $ionicLoading.hide();
                    $state.go('app.person_food', { person_id: data.person.id });
                })
                .error(function(data, status, header, config) {

                });
            } catch(erro) {
                $ionicPopup.alert({
                    title: 'Aviso!!!',
                    template: erro,
                    buttons: [{
                            text: 'ok',
                            type: 'button-calm',
                        }
                    ]
                });
                $ionicLoading.hide();
            }
        }

        $scope.save = function(id) {
            try{
                $ionicLoading.show({ template: 'Aguarde...' });

                if ($scope.food.image == 'img/upload/icon-camera.png' || $scope.food.image == photo_ant) {
                    photo_img = '';
                } else {
                    photo_img = $scope.food.image;
                }

                var data = $.param({
                    user_id: user.id,
                    user_token: user.token,
                    food_id: id,
                    title: $scope.food.title,
                    description: $scope.food.description,
                    image: photo_img
                });

                var config = {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                    }
                }

                $http.post('http://realper.filiperaiz.com.br/api/v1/realper/food_save.json', data, config)
                .success(function(data, status, headers) {

                    if (typeof data.errors_food == "undefined") {
                        $ionicLoading.hide();
                        $state.go('app.person_food', { person_id: data.person.id });
                    } else {
                        var er = '';
                        for (i = 0; i < data.errors_food.length; i++) {
                            er += data.errors_food[i].message + '<br>';
                        }
                        $ionicPopup.alert({
                            title: 'Aviso!!!',
                            template: er,
                            buttons: [{
                                    text: 'ok',
                                    type: 'button-calm',
                                }
                            ]
                        });
                        $ionicLoading.hide();
                    }
                })
                .error(function(data, status, header, config) {

                });
            } catch(erro) {
                $ionicPopup.alert({
                    title: 'Aviso!!!',
                    template: erro,
                    buttons: [{
                            text: 'ok',
                            type: 'button-calm',
                        }
                    ]
                });
                $ionicLoading.hide();
            }
        }

        $scope.takePhoto = function() {
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.CAMERA,
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 300,
                targetHeight: 300,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
            };

            $cordovaCamera.getPicture(options).then(function(imageData) {
                $scope.food.image = "data:image/jpeg;base64," + imageData;
            }, function(err) {
                // An error occured. Show a message to the user
            });
        }

        $scope.choosePhoto = function() {
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 300,
                targetHeight: 300,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
            };

            $cordovaCamera.getPicture(options).then(function(imageData) {
                $scope.food.image = "data:image/jpeg;base64," + imageData;
            }, function(err) {
                // An error occured. Show a message to the user
            });
        }

    } else {
        $ionicLoading.hide();
        $state.go('login');
    }
}])

.controller('itemPersonFoodNewCtrl', ['$state', '$scope', '$stateParams', '$cordovaCamera', '$ionicScrollDelegate', '$http', 'Util', '$window', '$ionicLoading', '$ionicPopup', function($state, $scope, $stateParams, $cordovaCamera, $ionicScrollDelegate, $http, Util, $window, $ionicLoading, $ionicPopup) {

    if (Util.logged()) {

        $scope.food = {};

        var user = JSON.parse($window.localStorage['user_token']);
        var person_id = $stateParams.person_id;


        $scope.save = function() {
            try{
                $ionicLoading.show({ template: 'Aguarde...' });
                var data = $.param({
                    user_id: user.id,
                    user_token: user.token,
                    title: $scope.food.title,
                    description: $scope.food.description,
                    image: $scope.food.image,
                    person_id: person_id
                });

                var config = {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                    }
                }

                $http.post('http://realper.filiperaiz.com.br/api/v1/realper/food_save.json', data, config)
                .success(function(data, status, headers) {
                    if (typeof data.errors_food == "undefined") {
                        $scope.medalalert(data.food_quant.quant);
                        $ionicLoading.hide();
                        $state.go('app.person_food', { person_id: data.person.id });
                    } else {
                        var er = '';
                        for (i = 0; i < data.errors_food.length; i++) {
                            er += data.errors_food[i].message + '<br>';
                        }
                        $ionicPopup.alert({
                            title: 'Aviso!!!',
                            template: er,
                            buttons: [{
                                    text: 'ok',
                                    type: 'button-calm',
                                }
                            ]
                        });
                        $ionicLoading.hide();
                    }
                })
                .error(function(data, status, header, config) {

                });
            } catch(erro) {
                $ionicPopup.alert({
                    title: 'Aviso!!!',
                    template: erro,
                    buttons: [{
                            text: 'ok',
                            type: 'button-calm',
                        }
                    ]
                });
                $ionicLoading.hide();
            }
        }

        $scope.takePhoto = function() {
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.CAMERA,
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 300,
                targetHeight: 300,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
            };

            $cordovaCamera.getPicture(options).then(function(imageData) {
                $scope.food.image = "data:image/jpeg;base64," + imageData;
            }, function(err) {
                // An error occured. Show a message to the user
            });
        }

        $scope.choosePhoto = function() {
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 300,
                targetHeight: 300,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
            };

            $cordovaCamera.getPicture(options).then(function(imageData) {
                $scope.food.image = "data:image/jpeg;base64," + imageData;
            }, function(err) {
                // An error occured. Show a message to the user
            });
        }

        $scope.medalalert = function(quant) {
            var str_alert,flag=false;
            if(quant==1){
                str_alert = '<div class="medal-conquer"><h2>Parabéns</h2><h4>Você ganhou uma medalha</h4><img src="img/upload/degustador.png" alt=""><p>Degustador</p></div>';
                flag = true;
            }
            if(flag){
                var alertPopup = $ionicPopup.alert({
                    title: '',
                    template: str_alert,
                    buttons: [{
                            text: 'ok',
                            type: 'button-calm',
                        }
                    ]
                });
            }
        }

    } else {
        $ionicLoading.hide();
        $state.go('login');
    }
}])



// INTEREST
.controller('itemPersonInterestsCtrl', ['$state', '$scope', '$stateParams', '$cordovaCamera', '$ionicScrollDelegate', '$http', 'Util', '$window', '$ionicLoading', '$ionicPopup', function($state, $scope, $stateParams, $cordovaCamera, $ionicScrollDelegate, $http, Util, $window, $ionicLoading, $ionicPopup) {

    $scope.process = false;
    $scope.person_id = $stateParams.person_id;

    if (Util.logged()) {

        $scope.list_interests = Array();

        var user = JSON.parse($window.localStorage['user_token']);
        $scope.hasMoreData = true;
        $scope.page = 1;

        var parameters = {
            user_id: user.id,
            user_token: user.token,
            person_id: $scope.person_id,
            page: $scope.page
        };

        var config = {
            params: parameters
        };

        $http.get('http://realper.filiperaiz.com.br/api/v1/realper/list_interests.json', config)
        .success(function(data, status, headers, config) {
            if (data.user_logged.flag) {
                $scope.list_interests = data.list_interests;
                $scope.process = true;
                $ionicLoading.hide();
            } else {
                $window.localStorage.removeItem('user_token');
                $ionicLoading.hide();
                $state.go('login');
            }
        });


        $scope.loadMore = function() {
            //$ionicLoading.show({template: '<ion-spinner icon="spiral"></ion-spinner><br>Aguarde...'});
            $scope.page += 1;

            var parameters = {
                token_user: user.token,
                user_id: user.id,
                user_token: user.token,
                person_id: $scope.person_id,
                page: $scope.page
            };

            var config = {
                params: parameters
            };

            $http.get('http://realper.filiperaiz.com.br/api/v1/realper/list_interests.json', config)
            .success(function(data, status, headers, config) {
                if (data.user_logged.flag) {
                    if (data.list_interests.length == 0) {
                        $scope.hasMoreData = false;
                    }
                    for (i = 0; i < data.list_interests.length; i++) {
                        $scope.list_interests.push(data.list_interests[i]);
                    }
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                } else {
                    $window.localStorage.removeItem('user_token');
                    $ionicLoading.hide();
                    $state.go('login');
                }
            });
        };


    } else {
        $ionicLoading.hide();
        $state.go('login');
    }
}])

.controller('itemPersonInterestsIdCtrl',['$state', '$scope', '$stateParams', '$cordovaCamera', '$ionicScrollDelegate', '$http', 'Util', '$window', '$ionicLoading', '$ionicPopup', function($state, $scope, $stateParams, $cordovaCamera, $ionicScrollDelegate, $http, Util, $window, $ionicLoading, $ionicPopup) {

    $scope.interest_id = $stateParams.interest_id;
    var photo_ant;

    if (Util.logged()) {

        $scope.person = {};

        var user = JSON.parse($window.localStorage['user_token']);
        $scope.hasMoreData = true;

        var parameters = {
            user_id: user.id,
            user_token: user.token,
            interest_id: $scope.interest_id
        };

        var config = {
            params: parameters
        };

        $http.get('http://realper.filiperaiz.com.br/api/v1/realper/interest.json', config)
        .success(function(data, status, headers, config) {
            if (data.user_logged.flag) {
                $scope.interest = data.interest;
                photo_ant = data.interest.image;
                $ionicLoading.hide();
            } else {
                $window.localStorage.removeItem('user_token');
                $ionicLoading.hide();
                $state.go('login');
            }
        });


        $scope.delete = function(id) {
            try{
                $ionicLoading.show({ template: 'Aguarde...' });
                var parameters = {
                    user_id: user.id,
                    user_token: user.token,
                    interest_id: id
                };

                var config = {
                    params: parameters
                };

                $http.delete('http://realper.filiperaiz.com.br/api/v1/realper/interest_delete.json', config)
                .success(function(data, status, headers) {
                    $ionicLoading.hide();
                    $state.go('app.person_present', { person_id: data.person.id });
                })
                .error(function(data, status, header, config) {

                });
            } catch(erro) {
                $ionicPopup.alert({
                    title: 'Aviso!!!',
                    template: erro,
                    buttons: [{
                            text: 'ok',
                            type: 'button-calm',
                        }
                    ]
                });
                $ionicLoading.hide();
            }
        }

        $scope.save = function(id) {
            try{
                $ionicLoading.show({ template: 'Aguarde...' });

                if ($scope.interest.image == 'img/upload/icon-camera.png' || $scope.interest.image == photo_ant) {
                    photo_img = '';
                } else {
                    photo_img = $scope.interest.image;
                }

                var data = $.param({
                    user_id: user.id,
                    user_token: user.token,
                    interest_id: id,
                    title: $scope.interest.title,
                    description: $scope.interest.description,
                    image: photo_img
                });

                var config = {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                    }
                }

                $http.post('http://realper.filiperaiz.com.br/api/v1/realper/interest_save.json', data, config)
                .success(function(data, status, headers) {
                    if (typeof data.errors_interest == "undefined") {
                        $ionicLoading.hide();
                        $state.go('app.person_present', { person_id: data.person.id });
                    } else {
                        var er = '';
                        for (i = 0; i < data.errors_interest.length; i++) {
                            er += data.errors_interest[i].message + '<br>';
                        }
                        $ionicPopup.alert({
                            title: 'Aviso!!!',
                            template: er,
                            buttons: [{
                                    text: 'ok',
                                    type: 'button-calm',
                                }
                            ]
                        });
                        $ionicLoading.hide();
                    }
                })
                .error(function(data, status, header, config) {

                });

            } catch(erro) {
                $ionicPopup.alert({
                    title: 'Aviso!!!',
                    template: erro,
                    buttons: [{
                            text: 'ok',
                            type: 'button-calm',
                        }
                    ]
                });
                $ionicLoading.hide();
            }
        }

        $scope.takePhoto = function() {
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.CAMERA,
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 300,
                targetHeight: 300,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
            };

            $cordovaCamera.getPicture(options).then(function(imageData) {
                $scope.interest.image = "data:image/jpeg;base64," + imageData;
            }, function(err) {
                // An error occured. Show a message to the user
            });
        }

        $scope.choosePhoto = function() {
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 300,
                targetHeight: 300,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
            };

            $cordovaCamera.getPicture(options).then(function(imageData) {
                $scope.interest.image = "data:image/jpeg;base64," + imageData;
            }, function(err) {
                // An error occured. Show a message to the user
            });
        }

    } else {
        $ionicLoading.hide();
        $state.go('login');
    }
}])

.controller('itemPersonInterestsNewCtrl', ['$state', '$scope', '$stateParams', '$cordovaCamera', '$ionicScrollDelegate', '$http', 'Util', '$window', '$ionicLoading', '$ionicPopup', function($state, $scope, $stateParams, $cordovaCamera, $ionicScrollDelegate, $http, Util, $window, $ionicLoading, $ionicPopup) {

    if (Util.logged()) {

        $scope.interest = {};

        var user = JSON.parse($window.localStorage['user_token']);
        var person_id = $stateParams.person_id;


        $scope.save = function() {
            try{
                $ionicLoading.show({ template: 'Aguarde...' });
                var data = $.param({
                    user_id: user.id,
                    user_token: user.token,
                    title: $scope.interest.title,
                    description: $scope.interest.description,
                    image: $scope.interest.image,
                    person_id: person_id
                });

                var config = {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                    }
                }

                $http.post('http://realper.filiperaiz.com.br/api/v1/realper/interest_save.json', data, config)
                .success(function(data, status, headers) {

                    if (typeof data.errors_interest == "undefined") {
                        $scope.medalalert(data.interest_quant.quant);
                        $ionicLoading.hide();
                        $state.go('app.person_present', { person_id: data.person.id });
                    } else {
                        var er = '';
                        for (i = 0; i < data.errors_interest.length; i++) {
                            er += data.errors_interest[i].message + '<br>';
                        }
                        $ionicPopup.alert({
                            title: 'Aviso!!!',
                            template: er,
                            buttons: [{
                                    text: 'ok',
                                    type: 'button-calm',
                                }
                            ]
                        });
                        $ionicLoading.hide();
                    }
                })
                .error(function(data, status, header, config) {

                });
            } catch(erro) {
                $ionicPopup.alert({
                    title: 'Aviso!!!',
                    template: erro,
                    buttons: [{
                            text: 'ok',
                            type: 'button-calm',
                        }
                    ]
                });
                $ionicLoading.hide();
            }
        }

        $scope.takePhoto = function() {
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.CAMERA,
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 300,
                targetHeight: 300,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
            };

            $cordovaCamera.getPicture(options).then(function(imageData) {
                $scope.interest.image = "data:image/jpeg;base64," + imageData;
            }, function(err) {
                // An error occured. Show a message to the user
            });
        }

        $scope.choosePhoto = function() {
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 300,
                targetHeight: 300,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
            };

            $cordovaCamera.getPicture(options).then(function(imageData) {
                $scope.interest.image = "data:image/jpeg;base64," + imageData;
            }, function(err) {
                // An error occured. Show a message to the user
            });
        }

        $scope.medalalert = function(quant) {
            var str_alert,flag=false;
            if(quant==1){
                str_alert = '<div class="medal-conquer"><h2>Parabéns</h2><h4>Você ganhou uma medalha</h4><img src="img/upload/atencioso.png" alt=""><p>atencioso</p></div>';
                flag = true;
            }
            if(flag){
                var alertPopup = $ionicPopup.alert({
                    title: '',
                    template: str_alert,
                    buttons: [{
                            text: 'ok',
                            type: 'button-calm',
                        }
                    ]
                });
            }
        }

    } else {
        $ionicLoading.hide();
        $state.go('login');
    }
}])


.controller('sendPicCtrl', ['$scope', '$window', '$state', '$stateParams', '$cordovaCamera', '$http', 'Util', '$ionicLoading', '$ionicPopup', function($scope, $window, $state, $stateParams, $cordovaCamera, $http, Util, $ionicLoading, $ionicPopup) {

    if (Util.logged()) {

        var user = JSON.parse($window.localStorage['user_token']);

        $scope.element = {};

        var parameters = {
            user_id: user.id,
            user_token: user.token,
            flag_all: true
        };

        var config = {
            params: parameters
        };

        $http.get('http://realper.filiperaiz.com.br/api/v1/realper/list_people.json', config)
        .success(function(data, status, headers, config) {
            if (data.user_logged.flag) {
                $scope.list_people = data.list_people;
                $ionicLoading.hide();
            } else {
                $window.localStorage.removeItem('user_token');
                $ionicLoading.hide();
                $state.go('login');
            }
        });


        $scope.save = function() {
            try{
                if (validate()) {
                    $ionicLoading.show({ template: 'Aguarde...' });
                    var data = $.param({
                        user_id: user.id,
                        user_token: user.token,
                        title: $scope.element.title,
                        description: $scope.element.description,
                        image: $scope.element.image,
                        person_id: $scope.element.person_id
                    });


                    var config = {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                        }
                    }

                    if ($scope.element.type == 'comidas') {
                        var url = 'http://realper.filiperaiz.com.br/api/v1/realper/food_save.json';
                    } else if ($scope.element.type == 'presentes') {
                        var url = 'http://realper.filiperaiz.com.br/api/v1/realper/interest_save.json';
                    } else if ($scope.element.type == 'lugares') {
                        var url = 'http://realper.filiperaiz.com.br/api/v1/realper/place_save.json';
                    }

                    $http.post(url, data, config)
                    .success(function(data, status, headers) {

                        if ($scope.element.type == 'comidas') {
                            errors = data.errors_food;
                        } else if ($scope.element.type == 'presentes') {
                            errors = data.errors_interest;
                        } else if ($scope.element.type == 'lugares') {
                            errors = data.errors_place;
                        }


                        if (typeof errors == "undefined") {
                            $ionicLoading.hide();

                            if ($scope.element.type == 'comidas') {
                                $state.go('app.person_food', { person_id: data.person.id });
                            } else if ($scope.element.type == 'presentes') {
                                $state.go('app.person_present', { person_id: data.person.id });
                            } else if ($scope.element.type == 'lugares') {
                                $state.go('app.person_place', { person_id: data.person.id });
                            }
                        } else {
                            var er = '';
                            for (i = 0; i < errors.length; i++) {
                                er += errors[i].message + '<br>';
                            }
                            $ionicPopup.alert({
                                title: 'Aviso!!!',
                                template: er,
                                buttons: [{
                                        text: 'ok',
                                        type: 'button-calm',
                                    }
                                ]
                            });
                            $ionicLoading.hide();
                        }
                    })
                    .error(function(data, status, header, config) {

                    });
                }
            } catch(erro) {
                $ionicPopup.alert({
                    title: 'Aviso!!!',
                    template: erro,
                    buttons: [{
                            text: 'ok',
                            type: 'button-calm',
                        }
                    ]
                });
                $ionicLoading.hide();
            }
        }

        validate = function() {
            flag_val = false;
            if (Util.emptyVal($scope.element.type)) {
                $ionicPopup.alert({
                    title: 'Aviso!!!',
                    template: 'Escolha uma categoria',
                    buttons: [{
                            text: 'ok',
                            type: 'button-calm',
                        }
                    ]
                });
            } else if (Util.emptyVal($scope.element.person_id)) {
                $ionicPopup.alert({
                    title: 'Aviso!!!',
                    template: 'Escolha uma pessoa',
                    buttons: [{
                            text: 'ok',
                            type: 'button-calm',
                        }
                    ]
                });
            } else {
                flag_val = true;
            }
            return flag_val;
        }

        $scope.takePhoto = function() {
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.CAMERA,
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 300,
                targetHeight: 300,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
            };

            $cordovaCamera.getPicture(options).then(function(imageData) {
                $scope.element.image = "data:image/jpeg;base64," + imageData;
            }, function(err) {
                // An error occured. Show a message to the user
            });
        }

        $scope.choosePhoto = function() {
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 300,
                targetHeight: 300,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
            };

            $cordovaCamera.getPicture(options).then(function(imageData) {
                $scope.element.image = "data:image/jpeg;base64," + imageData;
            }, function(err) {
                // An error occured. Show a message to the user
            });
        }

    } else {
        $ionicLoading.hide();
        $state.go('login');
    }
}])


// PROFILE
.controller('profileCtrl', ['$scope', '$stateParams', '$cordovaCamera', 'Util', '$window', '$http', '$ionicLoading', '$state', '$cordovaCamera', '$ionicPopup', function($scope, $stateParams, $cordovaCamera, Util, $window, $http, $ionicLoading, $state, $cordovaCamera, $ionicPopup) {


    var photo_ant;

    if (Util.logged()) {

        //$("#phone_n").mask('(99) 99999-9999');

        var user = JSON.parse($window.localStorage['user_token']);

        $scope.user_edit = {};

        var parameters = {
            user_id: user.id,
            user_token: user.token
        };

        var config = {
            params: parameters
        };

        $http.get('http://realper.filiperaiz.com.br/api/v1/realper/profile.json', config)
        .success(function(data, status, headers, config) {
            if (data.user_logged.flag) {
                $scope.user_edit = data.profile;
                photo_ant = data.profile.image;
                $ionicLoading.hide();
            } else {
                $window.localStorage.removeItem('user_token');
                $ionicLoading.hide();
                $state.go('login');
            }
        });


        $scope.save = function(id) {
            try{
                $ionicLoading.show({ template: 'Aguarde...' });

                if ($scope.user_edit.avatar == 'img/upload/icon-camera.png' || $scope.user_edit.image == photo_ant) {
                    photo_img = '';
                } else {
                    photo_img = $scope.user_edit.image;
                }

                var data = $.param({
                    user_id: user.id,
                    user_token: user.token,
                    name: $scope.user_edit.name,
                    phone_number: $scope.user_edit.phone_number,
                    image: photo_img,
                    password:$scope.user_edit.password,
                    password_confirmation:$scope.user_edit.password_confimated
                });

                var config = {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                    }
                }

                $http.post('http://realper.filiperaiz.com.br/api/v1/realper/profile_edit.json', data, config)
                .success(function(data, status, headers) {

                    if (typeof data.errors_user == "undefined") {

                        $window.localStorage['user_token'] = JSON.stringify(data.user);

                        $scope.$emit("menuUser", "dataMenu");


                        $ionicLoading.hide();

                        $ionicPopup.alert({
                            title: 'Aviso!!!',
                            template: 'Alterações Salvas!!!',
                            buttons: [{
                                    text: 'ok',
                                    type: 'button-calm',
                                }
                            ]
                        });

                        //$state.go('app.person');
                    } else {
                        var er = '';
                        for (i = 0; i < data.errors_user.length; i++) {
                            er += data.errors_user[i].message + '<br>';
                        }
                        $ionicPopup.alert({
                            title: 'Aviso!!!',
                            template: er,
                            buttons: [{
                                    text: 'ok',
                                    type: 'button-calm',
                                }
                            ]
                        });
                        $ionicLoading.hide();
                    }
                })
                .error(function(data, status, header, config) {
                     $ionicPopup.alert({
                        title: 'Aviso!!!',
                        template: 'Ocorreu um problema, tente mais tarde!!!',
                        buttons: [{
                                text: 'ok',
                                type: 'button-calm',
                            }
                        ]
                    });
                    $ionicLoading.hide();
                });
            } catch(erro) {
                $ionicPopup.alert({
                    title: 'Aviso!!!',
                    template: erro,
                    buttons: [{
                            text: 'ok',
                            type: 'button-calm',
                        }
                    ]
                });
                $ionicLoading.hide();
            }
        }

        $scope.takePhoto = function() {
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.CAMERA,
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 300,
                targetHeight: 300,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
            };

            $cordovaCamera.getPicture(options).then(function(imageData) {
                $scope.user_edit.image = "data:image/jpeg;base64," + imageData;
            }, function(err) {
                // An error occured. Show a message to the user
            });
        }

        $scope.choosePhoto = function() {
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 300,
                targetHeight: 300,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
            };

            $cordovaCamera.getPicture(options).then(function(imageData) {
                $scope.user_edit.image = "data:image/jpeg;base64," + imageData;
            }, function(err) {
                // An error occured. Show a message to the user
            });
        }

        $scope.mascara = function(){
            //alert('--')
            mask = "## #####-####";
            if($scope.user_edit.phone_number !== null && $scope.user_edit.phone_number !== undefined){
                var i = $scope.user_edit.phone_number.length;
                var saida = mask.substring(1,0);
                var texto = mask.substring(i)
                if (texto.substring(0,1) != saida){
                    $scope.user_edit.phone_number+= texto.substring(0,1);
                }
            }
        }

    } else {
        $ionicLoading.hide();
        $state.go('login');
    }
}])


// CONQUER
.controller('conquerCtrl', ['$scope', '$stateParams', '$cordovaCamera', 'Util', '$window', '$http', '$ionicLoading', '$state', '$cordovaCamera', '$ionicPopup', function($scope, $stateParams, $cordovaCamera, Util, $window, $http, $ionicLoading, $state, $cordovaCamera, $ionicPopup) {

    if (Util.logged()) {

        var user = JSON.parse($window.localStorage['user_token']);

        var parameters = {
            user_id: user.id,
            user_token: user.token
        };

        var config = {
            params: parameters
        };

        $http.get('http://realper.filiperaiz.com.br/api/v1/realper/conquer.json', config)
        .success(function(data, status, headers, config) {
            if (data.user_logged.flag) {
                $scope.conquer = data.conquer;
                $ionicLoading.hide();
            } else {
                $window.localStorage.removeItem('user_token');
                $ionicLoading.hide();
                $state.go('login');
            }
        });

    } else {
        $ionicLoading.hide();
        $state.go('login');
    }
}])